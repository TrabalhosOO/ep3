class CreateHotels < ActiveRecord::Migration[5.2]
  def change
    create_table :hotels do |t|
      t.string :foto
      t.string :nome
      t.integer :estrelas
      t.integer :avaliacao
      t.string :local
      t.integer :qtd_camas
      t.integer :qnt_banheiros
      t.string :piscina
      t.string :cafe
      t.integer :preço

      t.timestamps
    end
  end
end
