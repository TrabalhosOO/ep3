json.extract! hotel, :id, :foto, :nome, :estrelas, :avaliacao, :local, :qtd_camas, :qnt_banheiros, :piscina, :cafe, :preço, :created_at, :updated_at
json.url hotel_url(hotel, format: :json)
