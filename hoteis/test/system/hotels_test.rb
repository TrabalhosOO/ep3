require "application_system_test_case"

class HotelsTest < ApplicationSystemTestCase
  setup do
    @hotel = hotels(:one)
  end

  test "visiting the index" do
    visit hotels_url
    assert_selector "h1", text: "Hotels"
  end

  test "creating a Hotel" do
    visit hotels_url
    click_on "New Hotel"

    fill_in "Avaliacao", with: @hotel.avaliacao
    fill_in "Cafe", with: @hotel.cafe
    fill_in "Estrelas", with: @hotel.estrelas
    fill_in "Foto", with: @hotel.foto
    fill_in "Local", with: @hotel.local
    fill_in "Nome", with: @hotel.nome
    fill_in "Piscina", with: @hotel.piscina
    fill_in "Preço", with: @hotel.preço
    fill_in "Qnt Banheiros", with: @hotel.qnt_banheiros
    fill_in "Qtd Camas", with: @hotel.qtd_camas
    click_on "Create Hotel"

    assert_text "Hotel was successfully created"
    click_on "Back"
  end

  test "updating a Hotel" do
    visit hotels_url
    click_on "Edit", match: :first

    fill_in "Avaliacao", with: @hotel.avaliacao
    fill_in "Cafe", with: @hotel.cafe
    fill_in "Estrelas", with: @hotel.estrelas
    fill_in "Foto", with: @hotel.foto
    fill_in "Local", with: @hotel.local
    fill_in "Nome", with: @hotel.nome
    fill_in "Piscina", with: @hotel.piscina
    fill_in "Preço", with: @hotel.preço
    fill_in "Qnt Banheiros", with: @hotel.qnt_banheiros
    fill_in "Qtd Camas", with: @hotel.qtd_camas
    click_on "Update Hotel"

    assert_text "Hotel was successfully updated"
    click_on "Back"
  end

  test "destroying a Hotel" do
    visit hotels_url
    page.accept_confirm do
      click_on "Destroy", match: :first
    end

    assert_text "Hotel was successfully destroyed"
  end
end
