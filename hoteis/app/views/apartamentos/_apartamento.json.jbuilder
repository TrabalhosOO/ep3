json.extract! apartamento, :id, :tamanho, :categoria, :camas, :banheiros, :created_at, :updated_at
json.url apartamento_url(apartamento, format: :json)
