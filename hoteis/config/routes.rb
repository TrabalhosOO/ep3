Rails.application.routes.draw do
  resources :apartamentos
  devise_for :empresas
  devise_for :clientes
  resources :hotels do
    resources :comments
  end
  resources :hotel_empresas
  resources :logins
  resources :cadastros
  mount Commontator::Engine => '/commontator'
  get 'main/index'

  root to: 'main#index'

  # For details on the DSL available within this file, see http://guides.rubyonrails.org/routing.html
end
