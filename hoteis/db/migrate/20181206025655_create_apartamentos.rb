class CreateApartamentos < ActiveRecord::Migration[5.2]
  def change
    create_table :apartamentos do |t|
      t.integer :tamanho
      t.string :categoria
      t.integer :camas
      t.integer :banheiros

      t.timestamps
    end
  end
end
