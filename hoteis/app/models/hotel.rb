class Hotel < ApplicationRecord
    acts_as_commontable dependent: :destroy
    has_many :comments
    has_many :apartamentos
end
